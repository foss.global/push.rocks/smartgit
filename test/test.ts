import { tap, expect } from '@pushrocks/tapbundle';
import * as smartgit from '../ts/index.js';

import * as smartpath from '@pushrocks/smartpath';
const __dirname = smartpath.get.dirnameFromImportMetaUrl(import.meta.url);
import * as path from 'path';

let testSmartgitInstance: smartgit.Smartgit;
const testRepoDir = path.join(__dirname, '../.nogit/testrepo');
const testRepoDirSmartfile = path.join(__dirname, '../.nogit/pushrocks_smartfile');

tap.test('should create a valid smartgit instance', async () => {
  testSmartgitInstance = new smartgit.Smartgit();
  await testSmartgitInstance.init();
  expect(testSmartgitInstance).toBeInstanceOf(smartgit.Smartgit);
});

tap.test('should create a new repo at .nogit', async () => {
  const gitRepo = await testSmartgitInstance.createRepoByOpen(testRepoDir);
});

tap.test('should clone a repo', async () => {
  const gitRepo = await testSmartgitInstance.createRepoByClone(
    'https://gitlab.com/pushrocks/smartfile.git',
    testRepoDirSmartfile
  );
});

tap.start();

import * as plugins from './smartgit.plugins.js';

import { Smartgit } from './smartgit.classes.smartgit.js';

/**
 * class GitRepo allows access to git directories from node
 */
export class GitRepo {
  // STATIC
  /**
   * creates a new GitRepo Instance after cloning a project
   */
  public static async fromCloningIntoDir(
    smartgitRefArg: Smartgit,
    fromArg: string,
    toArg: string
  ): Promise<GitRepo> {
    const dirArg = plugins.path.resolve(toArg);
    await plugins.isomorphicGit.clone({
      dir: toArg,
      fs: smartgitRefArg.envDeps.fs,
      http: smartgitRefArg.envDeps.http,
      url: fromArg,
    });
    return new GitRepo(smartgitRefArg, toArg);
  }

  public static async fromCreatingRepoInDir(
    smartgitRefArg: Smartgit,
    dirArg: string
  ): Promise<GitRepo> {
    dirArg = plugins.path.resolve(dirArg);
    await plugins.isomorphicGit.init({
      dir: dirArg,
      fs: smartgitRefArg.envDeps.fs,
    });
    return new GitRepo(smartgitRefArg, dirArg);
  }

  public static async fromOpeningRepoDir(smartgitRefArg: Smartgit, dirArg: string) {
    dirArg = plugins.path.resolve(dirArg);
    return new GitRepo(smartgitRefArg, dirArg);
  }

  // INSTANCE
  public smartgitRef: Smartgit;
  public repoDir: string;

  constructor(smartgitRefArg: Smartgit, repoDirArg: string) {
    this.smartgitRef = smartgitRefArg;
    this.repoDir = repoDirArg;
  }

  /**
   * lists remotes
   */
  public async listRemotes(): Promise<
    {
      remote: string;
      url: string;
    }[]
  > {
    const remotes = await plugins.isomorphicGit.listRemotes({
      fs: this.smartgitRef.envDeps.fs,
      dir: this.repoDir,
    });
    return remotes;
  }

  /**
   * ensures the existance of a remote within a repository
   * @param remoteNameArg
   * @param remoteUrlArg
   */
  public async ensureRemote(remoteNameArg: string, remoteUrlArg: string): Promise<void> {
    const remotes = await this.listRemotes();
    const existingRemote = remotes.find((itemArg) => itemArg.remote === remoteNameArg);
    if (existingRemote) {
      if (existingRemote.url !== remoteUrlArg) {
        await plugins.isomorphicGit.deleteRemote({
          remote: remoteNameArg,
          fs: this.smartgitRef.envDeps.fs,
          dir: this.repoDir,
        });
      } else {
        return;
      }
    }
    await plugins.isomorphicGit.addRemote({
      remote: remoteNameArg,
      fs: this.smartgitRef.envDeps.fs,
      url: remoteUrlArg,
    });
  }

  /**
   * gets the url for a specific remote
   */
  public async getUrlForRemote(remoteName: string): Promise<string> {
    const remotes = await this.listRemotes();
    const existingRemote = remotes.find((remoteArg) => remoteArg.remote === remoteName);
    return existingRemote?.url;
  }

  public async pushBranchToRemote(branchName: string, remoteName: string) {
    await plugins.isomorphicGit.push({
      fs: this.smartgitRef.envDeps.fs,
      http: this.smartgitRef.envDeps.http,
      ref: branchName,
      remote: remoteName,
    });
  }
}
